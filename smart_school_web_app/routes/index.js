﻿'use strict';
var express = require('express');
const request = require('request');
const http = require('http');

var router = express.Router();
var FormData = require('form-data');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('data', {
        title: 'Node project'
        //,breadcrumbs     : req.breadcrumbs,
    });
});


router.get('/customerList', function (req, res, next) {
    res.render('./customer/customerList', {
        title: 'Customer List',
        contentHeader: "Customer List"
    });
});


router.get('/customer', function (req, res, next) {
    var customer = {}
    if (req.query.viewMode === 'ADD') {
        res.render('./customer/customerMaintenance', {
            title: 'Add New Customer',
            contentHeader: "Add New Customer",
            viewMode: 'ADD',
            customer: customer,
            formAction: 'http://localhost:1001/customer'
        });
    } else if (req.query.viewMode === 'EDIT') {
        callGetCustomerApi(req.query.id)
            .then(result => {
                res.render('./customer/customerMaintenance', {
                    title: 'Customer detail',
                    contentHeader: "Customer detail",
                    customer: result.body.data,
                    viewMode: 'EDIT',
                    formAction: 'http://localhost:1001/customer',
                    addAction: "http://localhost:3000/customer?viewMode=ADD",
                    editAction: `http://localhost:3000/customer?id=${req.query.id}&viewMode=EDIT`,
                    deleteAction: `http://localhost:1001/customer?id=${req.query.id}&viewMode=DELETE`,
                    cancelAction: `http://localhost:3000/customer?id=${req.query.id}`
                });
            })
            .catch(err => console.log(err));
    } else if (req.query.viewMode === 'VIEW') {

        callGetCustomerApi(req.query.id)
            .then(result => {
                res.render('./customer/customerMaintenance', {
                    title: 'Customer detail',
                    contentHeader: "Customer detail",
                    customer: result.body.data,
                    viewMode: 'VIEW',
                    formAction: 'http://localhost:1001/customer',
                    addAction: "http://localhost:3000/customer?viewMode=ADD",
                    editAction: `http://localhost:3000/customer?id=${req.query.id}&viewMode=EDIT`,
                    deleteAction: `http://localhost:3000/customer?id=${req.query.id}&viewMode=DELETE`,
                    cancelAction: `http://localhost:3000/customer?id=${req.query.id}`
                });
            })
            .catch(err => console.log(err));
    }

});



router.get('/mailBox', function (req, res, next) {
    res.render('mail_box', {
        title: 'Mail Box',
        contentHeader: "Mail Box"
    });
});

function callGetCustomerApi(_id) {
    return new Promise(function (resovle, reject) {
        request(`http://localhost:1001/customer?id=${_id}`, { json: true }, (err, res, body) => {
            if (err) { reject(err); }
            console.log(res.body.data);
            resovle(res);
        });
    });
}


function callUpdateCustomerApi(data) {
    console.log(formData);
    var formData = new FormData();
    var customer = {
        customerNumber: data.customerNumber,
        contactFirstName: data.contactFirstName,
        contactLastName: data.contactLastName,
        phone: data.phone,
        addressLine1: data.addressLine1,
        addressLine2: data.addressLine2,
        _id: data._id
    }
    return new Promise(function (resovle, reject) {
        var req = request.post('http://localhost:1001/customer', {
            headers: {
                'Content-Type' : 'application/json;charset=UTF-8'
            }, formData: JSON.stringify(customer)
        }, function optionalCallback(err, httpResponse, body) {
                if (err) {
                    console.error('update failed:', err);
                    reject(err);
                }
                console.log('update successful!  Server responded with:', body);
                resovle(body);
           });
        });
    }


function callUpdateCustomerApi_(data) {
    // An object of options to indicate where to post to
    var customer = {
        customerNumber: data.customerNumber,
        contactFirstName: data.contactFirstName,
        contactLastName: data.contactLastName,
        phone: data.phone,
        addressLine1: data.addressLine1,
        addressLine2: data.addressLine2,
        _id: data._id
    }

    var post_options = {
        host: 'http//:localhost',
        port: '1001',
        path: '/customer',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
        }
    };

    return new Promise(function (resovle, reject) {
        var post_req = https.request(post_options, function (res) {
            resovle(res.body);
        });

        post_req.write(customer);
        post_req.end
    });

}

module.exports = router;
