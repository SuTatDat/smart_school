function show_alert_success(html, title, body) {
    html.innerHTML = '';
        html.innerHTML += `<div class="alert alert-success alert-dismissible vi">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> ${title}</h4>
                ${body}
              </div>`;
}
