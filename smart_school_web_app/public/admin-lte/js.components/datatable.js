﻿function render_table (tableName, options) {
    $(function () {
        $(`${tableName}`).DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true,
            'ajax': {
                "url": options.ajax
            },
            "columns":
                options.columns

        })
    })
}

